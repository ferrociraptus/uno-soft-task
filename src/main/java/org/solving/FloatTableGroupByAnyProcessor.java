package org.solving;

import org.eclipse.collections.api.list.primitive.FloatList;
import org.eclipse.collections.api.list.primitive.IntList;
import org.eclipse.collections.api.list.primitive.MutableIntList;
import org.eclipse.collections.impl.list.mutable.primitive.IntArrayList;
import org.eclipse.collections.impl.map.mutable.primitive.FloatIntHashMap;
import org.eclipse.collections.impl.map.mutable.primitive.IntIntHashMap;
import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

public class FloatTableGroupByAnyProcessor {

    private final List<FloatList> table = new ArrayList<>();

    /*
     * If a group were merged into other it will be added as log to this map.
     * And a merged group is take value null
     *  (null set for saving the time to copy k+n values in arrayList for k removed group on remove operation).
     *
     * Using jump table saves enormous memory to store direct index or to store rows for invert index rebuild.
     * Additionally, using jump table eliminates the need to rebuild index after groups joints.
     * */
    private final IntIntHashMap jumpTable = new IntIntHashMap();
    // index value to group
    private final List<FloatIntHashMap> invertIndexes = new ArrayList<>(20);

    // indexed for hash join by each column for each row
    private final List<IntHashSet> groups = new ArrayList<>();

    // map row to their size
    public FloatTableGroupByAnyProcessor(){}

    /**
     * Handle row to build index and collect the groups by mapping group to line number
     * */
    public void processRow(FloatList row){
        table.add(row);

        if (row == null){
            return;
        }

        IntList rowGroups = getRowGroups(row);

        if (rowGroups.isEmpty()){
            int newGroupNum = createNewGroup();
            addRowToGroup(row, newGroupNum);
        }else if (rowGroups.size() == 1){
            int groupNum = rowGroups.get(0);
            addRowToGroup(row, groupNum);
        }else {
            int masterGroupNum = getRootGroup(rowGroups.get(0));
            // gi - group iter
            for (int gi = 1; gi < rowGroups.size(); gi++){
                int slaveGroupNum = rowGroups.get(gi);
                mergeGroups(masterGroupNum, slaveGroupNum);
            }
            addRowToGroup(row, masterGroupNum);
        }
    }

    public List<FloatList> getTable(){
        return table;
    }
    public Stream<IntHashSet> getGroupsStream(){
        return groups.stream().filter(Objects::nonNull);
    }

    /**
     * Find all groups that are connected by group by any column rule to {@param row}
     * */
    private IntList getRowGroups(FloatList row){
        MutableIntList containGroups = new IntArrayList(invertIndexes.size());

        int maxColumns = Math.min(row.size(), invertIndexes.size());
        for (int column = 0; column < maxColumns; column++) {
            float rowColumnValue = row.get(column);

            // ignore "null" row values
            if (rowColumnValue == Float.MIN_VALUE) {
                continue;
            }

            var index = invertIndexes.get(column);

            // buffering operation of searching value in map
            int indexValue = index.getIfAbsent(rowColumnValue, Integer.MAX_VALUE);
            if (indexValue != Integer.MAX_VALUE) {
                containGroups.add(indexValue);
            }
        }
        return containGroups;
    }

    public int getRowsAmount(){
        return table.size();
    }

    public int getColumnsAmount(){
        return invertIndexes.size();
    }

    /**
     * @return number of new created group
     * */
    private int createNewGroup(){
        IntHashSet newGroup = new IntHashSet();
        groups.add(newGroup);

        return groups.size() - 1;
    }

    private void addRowToGroup(FloatList row, int groupNum){
        expandIndexesToColumns(row.size());

        groupNum = getRootGroup(groupNum);

        for (int column = 0; column < row.size(); column++){
            float rowValue = row.get(column);
            if (rowValue == Float.MIN_VALUE)
                continue;

            invertIndexes.get(column).put(rowValue, groupNum);
        }
        groups.get(groupNum).add(table.size()-1);
    }

    /**
     * Create additional indexes if row has more columns than existed before.
     *
     * @param columnAmount checked value of requested columns amount
     */
    private void expandIndexesToColumns(int columnAmount){
        // create additional indexes if row has more columns than existed before
        for (
                int unExistedColumnI = invertIndexes.size();
                unExistedColumnI < columnAmount;
                unExistedColumnI++
        ){
            // big initial storage to minimize searching the element in index (minimize collision)
            // and minimize resize map operation if it eval.
            invertIndexes.add(new FloatIntHashMap(500000));
        }
    }

    /**
     * Move all rows from one group to another and slave group become virtual (linked to master)
     *
     * @param masterGroupNum where all rows were merged from the slave group
     * @param slaveGroupNum the group num identifier of a group which will be merged
     * */
    private void mergeGroups(int masterGroupNum, int slaveGroupNum){
        // get real groups
        masterGroupNum = getRootGroup(masterGroupNum);
        slaveGroupNum = getRootGroup(slaveGroupNum);

        if (slaveGroupNum != masterGroupNum){
            groups.get(masterGroupNum).addAll(groups.get(slaveGroupNum));

            /*
                set to null for save calculation time on remove operation (array copy from k+1 to n to k place)
                and mark a merged group as "link" group (by setting to null), to map all indexes from slave to master
             */
            groups.set(slaveGroupNum, null);
            jumpTable.put(slaveGroupNum, masterGroupNum);
        }
    }

    /**
     * @return the real existed group for {@param groupNum} if the group by groupNum is "virtual"
     * */
    private int getRootGroup(int groupNum){
        for (
                int tmpGroupNum = groupNum;
                tmpGroupNum != Integer.MAX_VALUE;
                tmpGroupNum = jumpTable.getIfAbsent(groupNum, Integer.MAX_VALUE)
        ){
            groupNum = tmpGroupNum;
        }

        return groupNum;
    }
}
