package org.solving;

import org.eclipse.collections.api.list.primitive.FloatList;
import org.eclipse.collections.api.list.primitive.MutableFloatList;
import org.eclipse.collections.impl.list.mutable.primitive.FloatArrayList;
import org.eclipse.collections.impl.set.mutable.primitive.IntHashSet;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class MainFloatTableProcessing {
    public static void main(String[] args){

        var scanner = new Scanner(System.in);
        scanner.nextInt();

        long startTime = System.currentTimeMillis();

        var tableProcessor = new FloatTableGroupByAnyProcessor();

        try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(args[0])))){
            reader.lines().map( line -> {

                        String[] columnTokens = line.split(";");

                        MutableFloatList rowValues = new FloatArrayList(columnTokens.length);
                        for (String s : columnTokens) {
                            float columnValue = Float.MIN_VALUE;

                            /*
                                After standard split, we have the possibilities to extract the number
                                All data values are like "79825116228" (phone number?!).
                                Thus, by substring extracting crop the " symbols,
                                because in all the long or other number type is more compact than number string
                                representation, and it is faster in comparing operation
                             */
                            var subStr = (s.startsWith("\"")) ? s.substring(1, s.length() - 1) : s;

                            // the main point is store the column index, if value empty mark by Integer.MIN_VALUE stub
                            if (!subStr.isBlank()) {
                                // 79825116228 can contain only long, not int
                                try {
                                    columnValue = Float.parseFloat(subStr);
                                } catch (NumberFormatException e) {
                                    // if row is prohibited, do not process row
                                    return null;
                                }
                            }
                            rowValues.add(columnValue);
                        }
                        return rowValues;
                    }
                    // ignore prohibited rows
            ).forEach(tableProcessor::processRow);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<FloatList> table = tableProcessor.getTable();
        List<IntHashSet> groups = tableProcessor.getGroupsStream()
                .sorted((g1, g2) -> {
                    // sorting by size
                    int cmp = g2.size() - g1.size();

                    // equal size groups sort by elements amount
                    if (cmp == 0){
                        int elementsAmountCmp = 0;
                        var groupIter = g2.intIterator();
                        while (groupIter.hasNext()){
                            elementsAmountCmp += table.get(groupIter.next()).size();
                        }
                        groupIter = g1.intIterator();
                        while (groupIter.hasNext()){
                            elementsAmountCmp -= table.get(groupIter.next()).size();
                        }
                        return elementsAmountCmp;
                    }
                    return cmp;
                }) // sort from biggest to lowest
                .toList();

        long algoTime = System.currentTimeMillis();

        // drop a hard link to processor for avoid memory leak and clear indexes heap
        tableProcessor = null;
        System.gc();


        /*
            Task output sound as:
            в) Вывести полученные группы в файл в следующем формате:

            Группа 1

            строчка1

            строчка2

            строчка3

            ...

            Группа 2

            строчка1

            строчка2

            строчка3

            В начале вывода указать получившиеся число групп с более чем одним элементом.
            Сверху расположить группы с наибольшим числом элементов.
        */
        long moreThenOneRowGroupsAmount = groups.stream().filter(g -> g.size() > 1).count();
        try ( var writer = new BufferedWriter(new FileWriter("answer.txt"), 1_000_000);
        ) {
            writer.write("Big groups amount (containing more than one row): " + moreThenOneRowGroupsAmount + "\n\n");

            for (int gi = 0; gi < groups.size(); gi++){
                var group = groups.get(gi);
                var groupIter = group.intIterator();
                Set<FloatList> groupRows = new HashSet<>(5);
                while (groupIter.hasNext()){
                    int rowNum = groupIter.next();

                    groupRows.add(table.get(rowNum));
                }

                writer.write("Группа " + (gi + 1) + "\n\n");

                // build row string with deserializing native value
                for (var groupRow : groupRows){
                    StringBuilder rowString = new StringBuilder();
                    for (int i = 0; i < groupRow.size(); i++){

                        double rowValue = groupRow.get(i);
                        // deserialize row data to row native string
                        rowString.append('"');
                        if (rowValue != Double.MIN_VALUE) {
                            /*
                                //if used test data memory optimizing, we use to revert the original value
                                rowString.append(79_000_000_000L + rowValue);
                            */
                            rowString.append(rowValue);
                        }
                        rowString.append('"');

                        if (i != groupRow.size() - 1){
                            rowString.append(';');
                        }
                    }
                    rowString.append("\n\n");
                    writer.write(rowString.toString());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        long endTime = System.currentTimeMillis();
        System.out.println("Full work time: " + (endTime - startTime) + "ms");
        System.out.println("\t- algorithm time (indexing): " + (algoTime - startTime) + "ms");
        System.out.println("\t- writing result to file: " + (endTime - algoTime) + "ms");
        System.out.println();
        System.out.println("Big groups amount (containing more than one row): " + moreThenOneRowGroupsAmount);
    }
}
