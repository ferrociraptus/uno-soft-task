package org.solving;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AlgorithmTest {

    private static final String testInputFilename = "test_in.txt";
    private static final String testOutputFilename = "answer.txt";

    @AfterAll
    static void removeTestFiles(){
        try {
            Files.delete(Path.of(testInputFilename));
            Files.delete(Path.of(testOutputFilename));
        } catch (Exception ignore){}
    }

    public static List<Set<String>> getGroupsFromFile(String fileName){
        try (var reader = new BufferedReader( new FileReader(fileName))) {
            List<Set<String>> groups = new ArrayList<>();
            final AtomicReference<Set<String>> group = new AtomicReference<>();
            reader.lines().forEach( line -> {
                    // ignore the first row
                    if (line.startsWith("B") || line.isEmpty()){
                        return;
                    }

                    if (line.startsWith("Группа")){
                        group.set(new HashSet<>());
                        groups.add(group.get());
                    }
                    else {
                        group.get().add(line);
                    }
                }
            );
            return groups;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static String arrToFileLine(long[] arr){
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < arr.length; i++){
            line.append('"');
            if (arr[i] != Long.MIN_VALUE){
                line.append(arr[i]);
            }
            line.append('"');
            if (i != arr.length - 1){
                line.append(';');
            }
        }
        return line.toString();
    }

    static String[] createTestFile(String filename, long[]... lines){
        var data = new String[lines.length];
        try ( var writer = new BufferedWriter(new FileWriter(filename), Short.MAX_VALUE);
        ) {
            for (int i = 0; i < lines.length; i++){
                var line = lines[i];
                var str = arrToFileLine(line);
                data[i] = str;
                writer.write(str);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return data;
    }

    static String[] createTestFile(String filename, String... lines){
        try ( var writer = new BufferedWriter(new FileWriter(filename), Short.MAX_VALUE);
        ) {
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }


    @Nested
    class IsolateGroups {

        @Test
        void single_one_row_group(){

            long[][] input = new long[][]{
                    {1, 2, 3},
            };

            createTestFile(testInputFilename, input);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertTrue(groups.get(0).contains(arrToFileLine(input[0])));
        }

        @Test
        void single_one_row_group_with_null_values(){

            String[] input = createTestFile(testInputFilename,
                    "\"1\";\"\";\"\"");

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertTrue(groups.get(0).contains(input[0]));
        }

        @Test
        void two_prohibited_rows(){

            createTestFile(testInputFilename,
                "\"8383\"200000741652251\"",
                    "\"79855053897\"83100000580443402\";\"200000133000191\""
            );

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(0, groups.size());
        }

        @Test
        void single_one_row_group_and_prohibited_rows(){

            String[] input = createTestFile(testInputFilename,
                    "\"8383\"200000741652251\"",
                    "\"79855053897\"83100000580443402\";\"200000133000191\"",
                    "\"1\";\"3\";\"5\""
            );

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertEquals(1, groups.get(0).size());
            assertTrue(groups.get(0).contains(input[2]));
        }

        @Test
        void two_row_group_one_column_join(){

            long[][] input = new long[][]{
                    {1, 2, 3},
                    {1, 5, 6}
            };

            createTestFile(testInputFilename, input);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertTrue(groups.get(0).contains(arrToFileLine(input[0])));
            assertTrue(groups.get(0).contains(arrToFileLine(input[1])));
        }

        @Test
        void two_row_group_one_column_join_with_null_values(){

            var input = createTestFile(testInputFilename,
                    "\"1\";\"2\";\"3\"",
                    "\"4\";\"\";\"3\""
                );

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertTrue(groups.get(0).contains(input[0]));
            assertTrue(groups.get(0).contains(input[1]));
        }

        @Test
        void two_row_group_plural_column_join(){

            long[][] input = new long[][]{
                    {1, 2, 3},
                    {1, 5, 3}
            };

            createTestFile(testInputFilename, input);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertTrue(groups.get(0).contains(arrToFileLine(input[0])));
            assertTrue(groups.get(0).contains(arrToFileLine(input[1])));
        }

        @Test
        void two_equals_row_group_plural_column_join(){

            long[][] input = new long[][]{
                    {1, 2, 3},
                    {1, 2, 3}
            };

            createTestFile(testInputFilename, input);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertEquals(1, groups.get(0).size());
            assertTrue(groups.get(0).contains(arrToFileLine(input[0])));
            assertTrue(groups.get(0).contains(arrToFileLine(input[1])));
        }

        @Test
        void single_one_row_groups(){

            long[][] inputData = new long[][]{
                    {1, 2, 3},
                    {3, 1, 6}
            };

            var input = createTestFile(testInputFilename, inputData);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(2, groups.size());
            // algorithm can shuffle the rows in groups, because used or


            assertTrue(
                (groups.get(0).contains(input[0]) && !groups.get(1).contains(input[0]))
                || (!groups.get(0).contains(input[0]) && groups.get(1).contains(input[0]))
            );
            assertTrue(
                (groups.get(0).contains(input[1]) && !groups.get(1).contains(input[1]))
                        || (!groups.get(0).contains(input[1]) && groups.get(1).contains(input[1]))
            );
        }
    }

    @Nested
    class GroupsMerging{

        @Test
        void one_shuffled_group(){

            long[][] inputData = new long[][]{
                    {1, 2, 3}, // 1 (3)
                    {4, 5, 6}, // 2 (3)
                    {0, 2, 7}, // 1 (3)
                    {8, 5, 9}, // 2 (3)
                    {1, 11, 9} // 1 + 2 (3)
            };

            String[] input = createTestFile(testInputFilename, inputData);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertEquals(5, groups.get(0).size());
        }

        @Test
        void one_shuffled_group_with_duplicate_rows(){

            long[][] inputData = new long[][]{
                    {1, 2, 3}, // 1 (3)
                    {4, 5, 6}, // 2 (3)
                    {1, 2, 3}, // 1 (3)
                    {8, 5, 9}, // 2 (3)
                    {1, 11, 9} // 1 + 2 (3)
            };

            String[] input = createTestFile(testInputFilename, inputData);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(1, groups.size());
            assertEquals(4, groups.get(0).size());
        }

        @Test
        void shuffled_two_groups(){

            long[][] inputData = new long[][]{
                    {1, 2, 3}, // 1
                    {4, 5, 6}, // 2
                    {0, 2, 7}, // 1
                    {8, 5, 9}, // 2
                    {10, 11, 9} // 2
            };

            String[] input = createTestFile(testInputFilename, inputData);

            MainLongTableProcessing.main(new String[]{testInputFilename});

            var groups = getGroupsFromFile(testOutputFilename);

            assertEquals(2, groups.size());
            // algorithm can shuffle the rows in groups, because used or


            Set<String> firstGroup = null;
            Set<String> secondGroup = null;
            if (groups.get(0).contains(arrToFileLine(inputData[0]))){
                firstGroup = groups.get(0);
                secondGroup = groups.get(1);
            } else {
                firstGroup = groups.get(1);
                secondGroup = groups.get(0);
            }

            assertEquals(2, firstGroup.size());
            assertEquals(3, secondGroup.size());

            assertTrue(firstGroup.contains(input[0]));
            assertTrue(firstGroup.contains(input[2]));

            assertTrue(secondGroup.contains(input[1]));
            assertTrue(secondGroup.contains(input[3]));
            assertTrue(secondGroup.contains(input[4]));
        }
    }
}

